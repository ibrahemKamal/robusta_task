<?php

use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Booking\BookingController;
use Illuminate\Support\Facades\Route;


Route::prefix('v1')
    ->group(function () {

        Route::post('login', [LoginController::class, 'login']);

        Route::middleware('auth:sanctum')->prefix('booking')
            ->controller(BookingController::class)
            ->group(function () {

                Route::get('cities', 'getCities');

                Route::get('available-seats', 'getAvailableSeats');

                Route::post('book-ride', 'bookRide');

            });

    });