<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    public function run()
    {
        $cities = collect(['Cairo', 'Giza', 'AlFayyum', 'AlMinya', 'Asyut'])
            ->map(function ($city) {
                return
                    [
                        'name' => $city,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
            })->toArray();

        City::insert($cities);
    }
}
