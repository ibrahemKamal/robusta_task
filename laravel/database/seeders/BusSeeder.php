<?php

namespace Database\Seeders;

use App\Models\Bus;
use App\Models\BusRide;
use App\Models\BusRideStation;
use App\Models\City;
use Illuminate\Database\Seeder;

class BusSeeder extends Seeder
{
    public function run()
    {
        // creating two buses and each bus has one ride and each ride has 3 stations
        Bus::factory(2)
            ->hasSeats(random_int(30,50))
            ->create()
            ->each(function (Bus $bus) {
                $cities = City::pluck('id');
                BusRide::factory(1, ['bus_id' => $bus->id])
                    ->create()
                    ->each(function (BusRide $busRide) use (&$order, &$cities) {
                        for ($i = 0; $i <= 3; $i++) {
                            $random_city = $cities->random();
                            BusRideStation::factory(1, [
                                'city_id' => $random_city,
                                'bus_ride_id' => $busRide->id,
                                'order' => $i
                            ])->create();
                            $cities->forget($cities->search($random_city));
                        }
                    });
            });
    }
}
