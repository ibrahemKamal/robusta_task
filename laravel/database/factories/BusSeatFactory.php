<?php

namespace Database\Factories;

use App\Models\BusSeat;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class BusSeatFactory extends Factory
{
    protected $model = BusSeat::class;

    public function definition(): array
    {
        return [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
