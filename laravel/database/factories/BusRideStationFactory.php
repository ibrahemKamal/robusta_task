<?php

namespace Database\Factories;

use App\Models\BusRideStation;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class BusRideStationFactory extends Factory
{
    protected $model = BusRideStation::class;

    public function definition(): array
    {
        return [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
