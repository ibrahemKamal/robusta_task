<?php

namespace Database\Factories;

use App\Models\BusRide;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class BusRideFactory extends Factory
{
    protected $model = BusRide::class;

    public function definition(): array
    {
        return [
            'ride_start_date' => Carbon::now(),
            'ride_end_date' => Carbon::now()->addHours($this->faker->numberBetween(1,9)),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
