<?php

use App\Models\BusRide;
use App\Models\City;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusRideStationsTable extends Migration
{
    public function up()
    {
        Schema::create('bus_ride_stations', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(BusRide::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->foreignIdFor(City::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->integer('order');


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bus_ride_stations');
    }
}