<?php

use App\Models\BusRide;
use App\Models\BusSeat;
use App\Models\City;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(BusRide::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->foreignIdFor(User::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->foreignIdFor(BusSeat::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->foreignId('start_station')
                ->constrained('cities')
                ->cascadeOnDelete();

            $table->foreignId('finish_station')
                ->constrained('cities')
                ->cascadeOnDelete();


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}