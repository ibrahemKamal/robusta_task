<?php

use App\Models\Bus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusRidesTable extends Migration
{
    public function up()
    {
        Schema::create('bus_rides', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Bus::class)
                ->constrained()
                ->cascadeOnDelete();


            $table->dateTime('ride_start_date');
            $table->dateTime('ride_end_date');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bus_rides');
    }
}