<?php

use App\Models\Booking;
use App\Models\BusRideStation;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingStationsTable extends Migration
{
    public function up()
    {
        Schema::create('booking_stations', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Booking::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->foreignIdFor(BusRideStation::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('booking_stations');
    }
}