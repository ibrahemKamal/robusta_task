<?php

namespace App\Http\Requests\Api\Booking;

use App\Http\Requests\ApiFormRequest;
use App\Models\BusSeat;
use App\Services\BookingService;

class BookRideRequest extends ApiFormRequest
{
    public function rules(): array
    {
        //we dont have to validate if the request data exist in the db here because we
        // will validate it in the withValidator hook
        return [
            'start_station' => 'required',
            'finish_station' => 'required',
            'seat_numbers' => 'required|array|max:10',
            'seat_numbers.*' => 'numeric',
            'ride_id' => 'required',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param \Illuminate\Validation\Validator $validator
     *
     * @return void
     */
    public function withValidator($validator): void
    {
        $validator->after(function ($validator) {
            $available_seats = $this->getAvailableSeats();
            foreach ($this->get('seat_numbers') as $index => $seat_number) {

                if (!in_array($seat_number, $available_seats)) {
                    $validator->errors()->add("seat_numbers.$index", 'This seat number is no available for this ride');
                }
            }

        });
    }

    protected function getAvailableSeats(): array
    {
        $busy_seats = app(BookingService::class)->setStartAndFinishStations($this->get('start_station'), $this->get('finish_station'))
            ->getBusySeats($this->get('ride_id'));
        return BusSeat::whereNotIn('id', $busy_seats)->pluck('id')->toArray();

    }
}