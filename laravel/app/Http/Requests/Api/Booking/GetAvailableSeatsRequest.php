<?php

namespace App\Http\Requests\Api\Booking;

use App\Http\Requests\ApiFormRequest;
use Illuminate\Foundation\Http\FormRequest;

class GetAvailableSeatsRequest extends ApiFormRequest
{
    public function rules(): array
    {
        return [
            'start_station' => 'required|exists:cities,id',
            'finish_station' => 'required|exists:cities,id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}