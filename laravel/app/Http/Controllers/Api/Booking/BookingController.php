<?php

namespace App\Http\Controllers\Api\Booking;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Booking\BookRideRequest;
use App\Http\Requests\Api\Booking\GetAvailableSeatsRequest;
use App\Http\Resources\BusRideResource;
use App\Http\Resources\CitiesResource;
use App\Libraries\ApiResponse;
use App\Models\Booking;
use App\Models\BookingStation;
use App\Models\BusRide;
use App\Models\BusRideStation;
use App\Models\City;
use App\Services\BookingService;

class BookingController extends Controller
{
    protected BookingService $bookingService;

    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    public function getCities()
    {
        return ApiResponse::success([
            'cities' => CitiesResource::collection(City::paginate())
        ]);
    }

    public function getAvailableSeats(GetAvailableSeatsRequest $request)
    {
        // the request should contain the day of the trip too but
        // just for now we gonna make it static as today()
        /**
         * lets break it down here
         *  1- get the busy seats
         *  2- get the bus rides that has the required stations
         *     which buses seats not in the busy seats array
         *     we use this two seperate queries cause subqueries are slower
         */

        $busy_seats = $this->bookingService
            ->setStartAndFinishStations($request->get('start_station'), $request->get('finish_station'))
        ->getBusySeats();

        $buses = BusRide::whereDay('ride_start_date', today())
            ->whereHas('stations', function ($stations_query) use ($busy_seats) {
                $stations_query->whereIn('city_id', [request('start_station'), request('finish_station')]);
            })
            ->with('bus', function ($bus_query) use ($busy_seats) {
                $bus_query->with('seats', function ($seats_query) use ($busy_seats) {
                    $seats_query->whereNotIn('id', $busy_seats);
                });
            })
            ->with('stations.city')
            ->withCount('stations')
            ->paginate();
        return BusRideResource::collection($buses);
    }


    public function bookRide(BookRideRequest $request)
    {
        $booking_data = collect($request->validated('seat_numbers'))
            ->map(function ($seat_number) use ($request) {
                return [
                    'bus_ride_id' => $request->validated('ride_id'),
                    'bus_seat_id' => $seat_number,
                    'start_station' => $request->validated('start_station'),
                    'finish_station' => $request->validated('finish_station'),
                ];
            })->toArray();
        $bookings = $request->user()->bookings()
            ->createMany($booking_data);
        $stations = BusRideStation::whereBetween('id', [$request->validated('start_station'), $request->validated('finish_station')])
            ->orderBy('order')->get('id');
        $booking_station_data = [];

        foreach ($bookings as $booking) {
            foreach ($stations as $station) {
                $booking_station_data[] = [
                    'bus_ride_station_id' => $station->id,
                    'booking_id' => $booking->id,
                    'created_at' => now(),
                    'updated_at' => now()
                ];
            }
        }
        BookingStation::insert($booking_station_data);
        return ApiResponse::success([], 201);
    }
}