<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\BusRide */
class BusRideResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ride_start_date' => $this->ride_start_date->toDateTimeString(),
            'ride_end_date' => $this->ride_end_date->toDateTimeString(),
            'stations_count' => $this->stations_count,
            'bus' => new BusResource($this->whenLoaded('bus')),
            'stations' => BusRideStationResource::collection($this->whenLoaded('stations')),
        ];
    }
}
