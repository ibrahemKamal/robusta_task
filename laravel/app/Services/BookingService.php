<?php

namespace App\Services;

use App\Models\Booking;
use App\Models\BookingStation;
use App\Models\BusRideStation;
use App\Models\BusSeat;

class BookingService
{
    protected $start_station;
    protected $finish_station;

    public function setStartAndFinishStations($start_station, $finish_station): static
    {
        $this->start_station = $start_station;
        $this->finish_station = $finish_station;
        return $this;
    }


    public function getStations(): array
    {
        return BusRideStation::whereBetween('id', [$this->start_station, $this->finish_station])
            ->orderBy('order')->pluck('id')->toArray();
    }


    public function getBusySeats($ride_id = null): array
    {
        $stations = $this->getStations();
        return Booking::query()
            ->when($ride_id, function ($query) use ($ride_id) {
                $query->where('bus_ride_id', $ride_id);
            })
            ->where(function ($query) {
                $query->where('finish_station', '!=', $this->start_station)
                    ->orWhere('start_station', $this->start_station);
            })
            ->whereHas('bookingStations', function ($booking_stations_query) use ($stations) {
                $booking_stations_query->whereIn('bus_ride_station_id', $stations);
            })
            ->distinct('bus_seat_id')
            ->pluck('bus_seat_id')
            ->toArray();
    }
}