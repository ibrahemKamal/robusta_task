<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BusRide extends Model
{
    use HasFactory;

    protected $fillable = ['bus_id', 'ride_start_date', 'ride_end_date'];

    protected $casts = [
        'ride_end_date' => 'datetime',
        'ride_start_date' => 'datetime'
    ];

    public function bus(): BelongsTo
    {
        return $this->belongsTo(Bus::class);
    }

    public function stations(): HasMany
    {
        return $this->hasMany(BusRideStation::class)
            ->orderBy('order');
    }

    public function bookings(): HasMany
    {
        return $this->hasMany(Booking::class);
    }
}