<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BookingStation extends Model
{
    protected $fillable = ['booking_id', 'bus_ride_station_id'];

    public function booking(): BelongsTo
    {
        return $this->belongsTo(Booking::class);
    }

    public function busRideStation(): BelongsTo
    {
        return $this->belongsTo(BusRideStation::class);
    }
}